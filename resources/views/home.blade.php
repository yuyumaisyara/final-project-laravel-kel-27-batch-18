@section('js')
<script type="text/javascript">
  $(document).ready(function() {
    $('#table').DataTable({
      "iDisplayLength": 50
    });

} );
</script>
@stop
@extends('layouts.app')

@section('content')
<div class="row">
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-right">
                <i class="mdi mdi-account-multiple text-primary icon-lg"></i>
              </div>
              <div class="float-left">
                <p class="mb-0 text-right">Member</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0">{{$anggota->count()}}</h3>
                </div>
              </div>
            </div>
            <p class="text-muted mt-3 mb-0">
              <i class="mdi mdi-account-multiple-plus mr-1" aria-hidden="true"></i> Total of all members
            </p>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-right">
                <i class="mdi mdi-book-open text-primary icon-lg" style="width: 40px;height: 40px;"></i>
              </div>
              <div class="float-left">
                <p class="mb-0 text-right">Book</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0">{{$buku->count()}}</h3>
                </div>
              </div>
            </div>
            <p class="text-muted mt-3 mb-0">
              <i class="mdi mdi-book-multiple mr-1" aria-hidden="true"></i> Total book titles
            </p>
          </div>
        </div>
      </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
            <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-right">
                      <i class="mdi mdi-folder-multiple text-primary icon-lg"></i>
                    </div>
                    <div class="float-left">
                      <p class="mb-0 text-right">Book Bending</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0">{{$transaksi->count()}}</h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-folder-move mr-1" aria-hidden="true"></i> Total of all Books Bending
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-right">
                      <i class="mdi mdi-check text-primary icon-lg"></i>
                    </div>
                    <div class="float-left">
                      <p class="mb-0 text-right">Currently Borrowing</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0">{{$transaksi->where('status', 'pinjam')->count()}}</h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-check-allr mr-1" aria-hidden="true"></i> Total books being borrowed
                  </p>
                </div>
              </div>
            </div>
           
            
</div>
<div class="row" >
<div class="col-lg-12 grid-margin stretch-card">
              <div class="card">

                <div class="card-body">
                  <h4 class="card-title">Borrowing Transaction Data</h4>
                  
                  <div class="table-responsive">
                    <table class="table table-striped" id="table">
                      <thead>
                        <tr>
                          <th>
                            Code
                          </th>
                          <th>
                            Book Title
                          </th>
                          <th>
                            Borrower
                          </th>
                          <th>
                            Borrow date
                          </th>
                          <th>
                            Date Back
                          </th>
                          <th>
                            Status
                          </th>
                          <th>
                            Action
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($datas as $data)
                        <tr>
                          <td class="py-1">
                          <a href="{{route('transaksi.show', $data->id)}}"> 
                            {{$data->kode_transaksi}}
                          </a>
                          </td>
                          <td>
                          
                            {{$data->buku->judul}}
                          
                          </td>

                          <td>
                            {{$data->anggota->nama}}
                          </td>
                          <td>
                           {{date('d/m/y', strtotime($data->tgl_pinjam))}}
                          </td>
                          <td>
                            {{date('d/m/y', strtotime($data->tgl_kembali))}}
                          </td>
                          <td>
                          @if($data->status == 'pinjam')
                            <label class="badge badge-warning">Borrow</label>
                          @else
                            <label class="badge badge-success">Return</label>
                          @endif
                          </td>
                          <td>
                          <form action="{{ route('transaksi.update', $data->id) }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('put') }}
                            <button class="btn btn-info btn-sm" onclick="return confirm('Are you sure this data is back?')">Already Back
                            </button>
                          </form>
                          
                          </td>
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
@endsection

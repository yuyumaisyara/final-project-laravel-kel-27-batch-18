<?php

use Illuminate\Database\Seeder;

class AnggotasTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    \App\Anggota::insert([
      [
        'id'        => 1,
        'user_id'      => 1,
        'npm'        => 151344401,
        'nama'       => 'Amanda Rahmat',
        'tempat_lahir'  => 'Bandung',
        'tgl_lahir'    => '2001-10-12',
        'jk'        => 'L',
        'prodi'      => 'TI',
        'created_at'      => \Carbon\Carbon::now(),
        'updated_at'      => \Carbon\Carbon::now()
      ],

    ]);
  }
}

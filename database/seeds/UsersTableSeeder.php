<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    \App\User::insert([
      [
        'id'        => 1,
        'name'        => 'AMANDA RAHMAT',
        'username'    => 'amandarahmat',
        'email'       => 'amanda@gmail.com',
        'password'    => bcrypt('amandarahmat'),
        'gambar'      => NULL,
        'level'      => 'admin',
        'remember_token'  => NULL,
        'created_at'      => \Carbon\Carbon::now(),
        'updated_at'      => \Carbon\Carbon::now()
      ],
      [
        'id'        => 2,
        'name'        => 'RAHMAT HIDAYAT',
        'username'    => 'rahmathidayat',
        'email'       => 'rahmat@gmail.com',
        'password'    => bcrypt('rahmathidayat'),
        'gambar'      => NULL,
        'level'      => 'user',
        'remember_token'  => NULL,
        'created_at'      => \Carbon\Carbon::now(),
        'updated_at'      => \Carbon\Carbon::now()
      ]
    ]);
  }
}

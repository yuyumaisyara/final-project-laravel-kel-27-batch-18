<?php

use Illuminate\Database\Seeder;

class BukusTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    \App\Buku::insert([
      [
        'id'        => 1,
        'judul'        => 'The Life-Changing Manga of Tidying Up: A Magical Story',
        'isbn'      => '3000406',
        'pengarang'     => 'Kondo, Marie',
        'penerbit'    => 'Ten Speed Press',
        'tahun_terbit'  => 2017,
        'jumlah_buku'    => 22,
        'deskripsi'    => 'From the #1 New York Times bestselling author and star of Netflixs Tidying Up with Marie Kondo, this graphic novelization brings Kondos life-changing tidying method to life with the fun, quirky story of a woman who transforms her home, work, and love life using Kondos advice and inspiration.',
        'lokasi'      => 'rak1',
        'cover'      => 'ini.jpg',
        'created_at'      => \Carbon\Carbon::now(),
        'updated_at'      => \Carbon\Carbon::now()
      ],

    ]);
  }
}
